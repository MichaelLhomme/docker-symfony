# Information

A docker container designed to run *symfony* for **development** purpose. Comes with an helper script `symfony` to run directly the commands within the container 

> !!! Please note that database servers won't be accessible from the container, instead use `sqlite` for the devlopment environment

# Usage

You can use the `symfony` helper script as the original `symfony` command. To start the web server, use `symfony serve` from your project directory, it will use the default port 8000 (you can change the port by editing the helper script)

Additional commands:

- `symfony shell` to start a shell in the container

# Pre-built image

If no adjustments are needed (pre-built image uses PHP 7.4), you can use the deployed image from the GitLab Container Registry. Simply download the helper scripts and move them somewhere accessible by your shell (eg: `/usr/local/bin`)

# Build the image

If you need to adjust the image (to change the PHP version), you can build the image with the following instructions. The helper script then needs to be adjusted to replace `registry.gitlab.com/michaellhomme/docker-symfony` by the name of the image you build (`symfony` in the following examples)

```
docker build -t symfony .
```
