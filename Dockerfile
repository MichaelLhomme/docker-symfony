FROM php:8-alpine

# Install tools
RUN apk update \
    # Linux tools
    && apk add bash \
    && apk add git \
    # Xdebug utils
    && apk add build-base linux-headers \
    && apk add autoconf \
    && pecl install xdebug \
    && docker-php-ext-enable xdebug \
    # Setup user environment
    && adduser user -D \
    && apk add shadow

# Install composer
RUN EXPECTED_CHECKSUM="$(php -r 'copy("https://composer.github.io/installer.sig", "php://stdout");')" && \
    php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" && \
    php -r "if (hash_file('sha384', 'composer-setup.php') === '$EXPECTED_CHECKSUM') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;" && \
    php composer-setup.php --install-dir=/usr/local/bin --filename=composer && \
    php -r "unlink('composer-setup.php');"

# Install symfony CLI
RUN wget https://get.symfony.com/cli/installer -O - | bash \
  && mv /root/.symfony5/bin/symfony /usr/local/bin/symfony

COPY symfony-su /usr/local/bin/symfony-su
RUN chmod a+x /usr/local/bin/symfony-su

# Setup container entry points
ENTRYPOINT [ "sh", "/usr/local/bin/symfony-su" ]
